import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:pokeapp/pokemon.dart';
import 'globals.dart' as globals;
import 'pokeViewPage.dart';
import 'pokemonDao.dart';


class PokeList extends StatefulWidget {
  PokeList({Key key, this.title, this.dao}) : super(key: key);

  final String title;
  final PokemonDao dao;

  @override
  _PokeListState createState() => _PokeListState();
}

class _PokeListState extends State<PokeList> {
  List<String> l = [];
  List<String> _filtredList = [];
  String _filter;
  bool _loading = true;
  int _timer = 0;

  TextEditingController _controller = TextEditingController();
  StreamSubscription _streamSub;

  @override
  void initState() {
    // TODO: implement initState
    _getList();
    super.initState();
  }

  _getList() async {
    l = [];
    _streamSub = Stream<int>.periodic(Duration(milliseconds: 100), (x) => x)
        .listen((data) {
      if (!_loading)
        setState(() {
          _streamSub.pause();
        });
      _timer++;
      if (_timer >= 30) {
        //print('Error: Data cannot be get');
        Navigator.of(context).pushReplacementNamed('/error');
        _streamSub.pause();
      }
    });
    Response tmp = await get('https://pokeapi.co/api/v2/pokemon?limit=1000');
    Map<String, dynamic> jsonObj = jsonDecode(tmp.body);
    jsonObj['results'].forEach((pokemon) {
      l.add(pokemon['name'].toString());
    });
    _filtredList = l;
    _loading = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          controller: _controller,
          decoration: InputDecoration(hintText: "Search for a Pokemon"),
          style: TextStyle(color: Colors.white),
          onChanged: (data) {
            _filter = data.toUpperCase();
            _filtredList = [];
            _filtredList = l.where((test) {
              return (test ?? '').toUpperCase().startsWith(_filter);
            }).toList();

            setState(() {});
          },
        ),
      ),
      body: _loading
          ? Center(
              child: Container(
                  //color: Colors.white,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        SizedBox(
                            height: 50,
                            width: 50,
                            child: CircularProgressIndicator())
                      ])))
          : Center(
              child: ListView.builder(
              itemCount: _filtredList.length,
              itemBuilder: (ctx, index) {
                return ListTile(
                  title: Text(_filtredList[index]),
                  onTap: (){
                    globals.selectedPokemon = _filtredList[index];
                    Navigator.of(context).pushNamed('/pokemon');
                    },
                );
              },
            )),
    );
  }
}
