// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorFlutterDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$FlutterDatabaseBuilder databaseBuilder(String name) =>
      _$FlutterDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$FlutterDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$FlutterDatabaseBuilder(null);
}

class _$FlutterDatabaseBuilder {
  _$FlutterDatabaseBuilder(this.name);

  final String name;

  final List<Migration> _migrations = [];

  Callback _callback;

  /// Adds migrations to the builder.
  _$FlutterDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$FlutterDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<FlutterDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name)
        : ':memory:';
    final database = _$FlutterDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$FlutterDatabase extends FlutterDatabase {
  _$FlutterDatabase([StreamController<String> listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  PokemonDao _pokemonDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Pokemon` (`name` TEXT, PRIMARY KEY (`name`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  PokemonDao get pokemonDao {
    return _pokemonDaoInstance ??= _$PokemonDao(database, changeListener);
  }
}

class _$PokemonDao extends PokemonDao {
  _$PokemonDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _pokemonInsertionAdapter = InsertionAdapter(
            database,
            'Pokemon',
            (Pokemon item) => <String, dynamic>{'name': item.name},
            changeListener),
        _pokemonUpdateAdapter = UpdateAdapter(
            database,
            'Pokemon',
            ['name'],
            (Pokemon item) => <String, dynamic>{'name': item.name},
            changeListener),
        _pokemonDeletionAdapter = DeletionAdapter(
            database,
            'Pokemon',
            ['name'],
            (Pokemon item) => <String, dynamic>{'name': item.name},
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _pokemonMapper =
      (Map<String, dynamic> row) => Pokemon(name: row['name'] as String);

  final InsertionAdapter<Pokemon> _pokemonInsertionAdapter;

  final UpdateAdapter<Pokemon> _pokemonUpdateAdapter;

  final DeletionAdapter<Pokemon> _pokemonDeletionAdapter;

  @override
  Future<Pokemon> findPokemonById(String id) async {
    return _queryAdapter.query('SELECT * FROM Pokemon WHERE name = ?',
        arguments: <dynamic>[id], mapper: _pokemonMapper);
  }

  @override
  Future<List<Pokemon>> findAllPokemons() async {
    return _queryAdapter.queryList('SELECT * FROM Pokemon',
        mapper: _pokemonMapper);
  }

  @override
  Stream<List<Pokemon>> findAllPokemonsAsStream() {
    return _queryAdapter.queryListStream('SELECT * FROM Pokemon',
        queryableName: 'Pokemon', isView: false, mapper: _pokemonMapper);
  }

  @override
  Future<void> deleteAllPokemons() async {
    await _queryAdapter.queryNoReturn('DELETE FROM Pokemon');
  }

  @override
  Future<void> insertPokemon(Pokemon pokemon) async {
    await _pokemonInsertionAdapter.insert(pokemon, OnConflictStrategy.abort);
  }

  @override
  Future<void> insertPokemons(List<Pokemon> pokemons) async {
    await _pokemonInsertionAdapter.insertList(
        pokemons, OnConflictStrategy.abort);
  }

  @override
  Future<void> updatePokemon(Pokemon pokemon) async {
    await _pokemonUpdateAdapter.update(pokemon, OnConflictStrategy.abort);
  }

  @override
  Future<void> updatePokemons(List<Pokemon> pokemon) async {
    await _pokemonUpdateAdapter.updateList(pokemon, OnConflictStrategy.abort);
  }

  @override
  Future<void> deletePokemon(Pokemon pokemon) async {
    await _pokemonDeletionAdapter.delete(pokemon);
  }

  @override
  Future<void> deletePokemons(List<Pokemon> pokemons) async {
    await _pokemonDeletionAdapter.deleteList(pokemons);
  }
}
