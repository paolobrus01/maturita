import 'dart:convert';

import 'package:http/http.dart';
import 'package:pokeapp/pokemonDao.dart';
import 'package:pokeapp/tipo.dart';

import 'pokemon.dart';

String selectedPokemon;

List<Pokemon> favourites;
Map<String,PokeType> types = {};

updateFavourites(PokemonDao dao) async{
  favourites = await dao.findAllPokemons();
}

bool favContains(String poke){
  bool answ = false;
  for(int i=0; i<favourites.length; i++){
    //print('${favourites[i].name} == $poke');
    if(favourites[i].name.toLowerCase() == poke.toLowerCase()) 
    {answ = true; break;}
  }

  return answ;
}

Future<void> getTypes() async{
  Response tmp = await get('https://pokeapi.co/api/v2/type');
  Map jsonObj = jsonDecode(tmp.body);
  List<dynamic> results = jsonObj['results'];
  results.forEach((f){
    types[f['name']] = PokeType.fromURL(f['name']);
  });

  //print(types);
  
}

String formatArray(List<dynamic> list){
  String tmp = list[0];
  for(int i=1; i<list.length; i++){
    tmp = '$tmp,  ${list[i]}';
  }
  return tmp;
}

String compareTypes(PokemonAdvanced p1, PokemonAdvanced p2){
  double punteggio = 0;
  print('${p1.types} SONO I TIPI');
  p1.types.forEach((f){
    p2.types.forEach((k){
      print('${p1.types} SONO I TIPI');
      if(types[f].double_damage_from.contains(k))
        punteggio-=40;
      if(types[f].double_damage_to.contains(k))
        punteggio+=40;

      if(types[f].half_damage_from.contains(k))
        punteggio+=30;
      if(types[f].half_damage_to.contains(k))
        punteggio-=30; 

      if(types[f].no_damage_from.contains(k))
        punteggio+=60;
      if(types[f].no_damage_to.contains(k))
        punteggio-=60; 
    });
  });

  if(p1.attack>p2.attack)
    punteggio+= 15;
  if(p2.attack>p1.attack)
    punteggio-= 15;

  if(p1.defense>p2.defense)
    punteggio+= 10;
  if(p2.defense>p1.defense)
    punteggio-= 10;

  if(p1.speed>p2.speed)
    punteggio+= 5;
  if(p2.speed>p1.speed)
    punteggio-= 5;

  if(p1.hp>p2.hp)
    punteggio+= 5;
  if(p2.hp>p1.hp)
    punteggio-= 5;

    return punteggio.toString();

}