import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'globals.dart' as globals;
import 'package:flutter/material.dart';
import 'package:pokeapp/pokemon.dart';

import 'myDrawer.dart';
import 'pokemonDao.dart';

class PokeViewPage extends StatefulWidget {
  PokeViewPage({Key key, this.title, this.dao}) : super(key: key);

  final String title;
  final PokemonDao dao;

  @override
  _PokeViewPageState createState() => _PokeViewPageState();
}

class _PokeViewPageState extends State<PokeViewPage> {
  StreamSubscription _streamSub;
  PokemonAdvanced _pokemon;
  Pokemon _pokemonBase;
  int _timer = 0;
  bool favourite;
  Future<List<String>> evolution_chain;
  String _description;

  Future<List<String>> _getEvolution() async {
    Response tmp = await get(_pokemon.specie);
    Map jsonObj = jsonDecode(tmp.body);
    setState(() {
      _description = (jsonObj['flavor_text_entries'][0]['flavor_text'])
          .toString()
          .replaceAll('\n', ' ');
      //print(_description);
    });
    Response tmp2 = await get(jsonObj['evolution_chain']['url']);
    jsonObj = jsonDecode(tmp2.body);
    Map<dynamic, dynamic> current = jsonObj['chain'];
    List<String> answer = [];
    answer.add(current['species']['name']);
    while (!current['evolves_to'].isEmpty) {
      current = current['evolves_to'][0];
      //print(current['evolves_to']);
      answer.add(current['species']['name']);
    }
    return answer;
  }

  @override
  void initState() {
    // TODO: implement initState
    _pokemon = PokemonAdvanced.fromID(widget.title);
    _pokemon.load();
    _pokemonBase = Pokemon(name: widget.title.toLowerCase());
    favourite = globals.favContains(widget.title);

    //favourite = widget.dao.findPokemonById(widget.title);
    super.initState();

    _streamSub = Stream<int>.periodic(Duration(milliseconds: 100), (x) => x)
        .listen((data) {
      if (_pokemon.loaded) {
        setState(() {
          _streamSub.pause();
        });
        evolution_chain = _getEvolution();
      }

      _timer++;
      if (_timer >= 30) {
        print('Error: Data cannot be get');
        Navigator.of(context).pushReplacementNamed('/error');
        _streamSub.pause();
      }
    });
  }

  _addPokemon() {
    favourite = true;
    setState(() {
      widget.dao.insertPokemon(_pokemonBase);
      globals.updateFavourites(widget.dao);
    });
  }

  _removePokemon() {
    favourite = false;
    setState(() {
      widget.dao.deletePokemon(_pokemonBase);
      globals.updateFavourites(widget.dao);
    });
  }

  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: myDrawer(context, this, widget.dao, controller),
      appBar: AppBar(
        title: Text(widget.title.toUpperCase()),
        actions: <Widget>[
          favourite
              ? IconButton(icon: Icon(Icons.star), onPressed: _removePokemon)
              : IconButton(
                  icon: Icon(Icons.star_border), onPressed: _addPokemon)
        ],
      ),
      body: (_pokemon.loaded)
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  //Text('${_pokemon.name}'),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          width: 200,
                          height: 200,
                          child: Image.network(_pokemon.spriteURL,
                              fit: BoxFit.fill)),
                      FutureBuilder(
                          future: evolution_chain,
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              return Container(
                                width: 200,
                                height: 200,
                                child: ListView.builder(
                                  padding: EdgeInsets.all(10),
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: snapshot.data.length,
                                  itemBuilder: (ctx, index) {
                                    return RaisedButton(
                                      onPressed: () {
                                        globals.selectedPokemon =
                                            snapshot.data[index];
                                        Navigator.of(context)
                                            .popAndPushNamed('/pokemon');
                                      },
                                      color: snapshot.data[index] ==
                                              widget.title.toLowerCase()
                                          ? Colors.red
                                          : Colors.white,
                                      child: Text(
                                        snapshot.data[index],
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: snapshot.data[index] ==
                                                    widget.title.toLowerCase()
                                                ? Colors.white
                                                : Colors.grey),
                                      ),
                                    );
                                  },
                                ),
                              );
                            }
                            return Container(
                                width: 200,
                                height: 200,
                                child: Padding(
                                    padding: EdgeInsets.all(75),
                                    child: CircularProgressIndicator()));
                          })
                    ],
                  ),
                  Text('types: ${globals.formatArray(_pokemon.types)}'),
                  Padding(padding: EdgeInsets.all(10)),
                  Container(
                      height: 100,
                      child: GridView.count(
                        crossAxisCount: 2,
                        physics: NeverScrollableScrollPhysics(),
                        childAspectRatio: 4,
                        children: <Widget>[
                          Card(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(MdiIcons.hospital),
                                Container(width: 5),
                                Text(_pokemon.hp.toString())
                              ],
                            ),
                          ),
                          Card(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(MdiIcons.sword),
                                Container(width: 5),
                                Text(_pokemon.attack.toString())
                              ],
                            ),
                          ),
                          Card(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(MdiIcons.shield),
                                Container(width: 5),
                                Text(_pokemon.defense.toString())
                              ],
                            ),
                          ),
                          Card(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(MdiIcons.runFast),
                                Container(width: 5),
                                Text(_pokemon.speed.toString())
                              ],
                            ),
                          )
                        ],
                      )),
                  Card(
                      child: Container(
                    child: ListTile(
                      subtitle: Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Text(
                            _description ?? "loading...",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 15),
                          )),
                      title: Text(
                        'Description',
                        textAlign: TextAlign.center,
                      ),
                    ),
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                  )),
                  Row(
                    children: <Widget>[
                      Flexible(
                          flex: 1,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(MdiIcons.humanMaleHeight),
                                  Container(width: 5),
                                  Text("${(_pokemon.height*10).toString()} cm")
                                ],
                              ),
                            ),
                          )),
                      Flexible(
                        child: Card(
                            child: Padding(
                                padding: EdgeInsets.symmetric(vertical: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(MdiIcons.weight),
                                    Container(width: 5),
                                    Text("${(_pokemon.weight/10).toString()} kg")
                                  ],
                                ))),
                      )
                    ],
                  )
                ],
              ),
            )
          : Center(
              child: Container(
                  //color: Colors.white,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                  SizedBox(
                      height: 50, width: 50, child: CircularProgressIndicator())
                ]))),
    );
  }
}
