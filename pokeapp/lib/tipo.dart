import 'dart:convert';

import 'package:http/http.dart';

class PokeType{
  String name;
  List<dynamic> double_damage_from = [];
  List<dynamic> double_damage_to = [];
  List<dynamic> half_damage_from = [];
  List<dynamic> half_damage_to = [];
  List<dynamic> no_damage_from = [];
  List<dynamic> no_damage_to = [];

  bool loaded = false;

  PokeType.fromURL(String s){
    _fetchData(s);
  }

  Future<Response> _fetchData(String s) async {
    //print('https://pokeapi.co/api/v2/pokemon/$s');
    Response tmp = await get('https://pokeapi.co/api/v2/type/$s');
    Map jsonObj = jsonDecode(tmp.body);

    this.name = jsonObj['name'];

    jsonObj['damage_relations']['double_damage_from'].forEach((tmp){this.double_damage_from.add(tmp['name']);});
    jsonObj['damage_relations']['double_damage_to'].forEach((tmp){this.double_damage_to.add(tmp['name']);});
    jsonObj['damage_relations']['half_damage_from'].forEach((tmp){this.half_damage_from.add(tmp['name']);});
    jsonObj['damage_relations']['half_damage_to'].forEach((tmp){this.half_damage_to.add(tmp['name']);});
    jsonObj['damage_relations']['no_damage_from'].forEach((tmp){this.no_damage_from.add(tmp['name']);});
    jsonObj['damage_relations']['no_damage_to'].forEach((tmp){this.no_damage_to.add(tmp['name']);});
    
    this.loaded = true;
    return tmp;
  }
}