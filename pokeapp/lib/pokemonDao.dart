import 'package:floor/floor.dart';
import 'pokemon.dart';

@dao
abstract class PokemonDao {
  @Query('SELECT * FROM Pokemon WHERE name = :id')
  Future<Pokemon> findPokemonById(String id);

  @Query('SELECT * FROM Pokemon')
  Future<List<Pokemon>> findAllPokemons();

  @Query('SELECT * FROM Pokemon')
  Stream<List<Pokemon>> findAllPokemonsAsStream();

  @insert
  Future<void> insertPokemon(Pokemon pokemon);

  @insert
  Future<void> insertPokemons(List<Pokemon> pokemons);

  @update
  Future<void> updatePokemon(Pokemon pokemon);

  @update
  Future<void> updatePokemons(List<Pokemon> pokemon);

  @delete
  Future<void> deletePokemon(Pokemon pokemon);

  @delete
  Future<void> deletePokemons(List<Pokemon> pokemons);

  @Query('DELETE FROM Pokemon')
  Future<void> deleteAllPokemons();
}