import 'dart:convert';

import 'package:floor/floor.dart';
import 'package:http/http.dart';

@entity
class Pokemon{
  @primaryKey
  String name = "";
  
  Pokemon({this.name});


}

class PokemonAdvanced {
  String name = "";
  String spriteURL;
  int hp,attack,defense,speed,height,weight,id; 
  String specie;
  List<String> types = [];
  //List types;
  bool loaded = false;

  PokemonAdvanced.fromID(String id){
   this.name = id;
  }

  PokemonAdvanced();

  Future<PokemonAdvanced> load() async{
    //await _fetchData(this.name);
    Response tmp = await get('https://pokeapi.co/api/v2/pokemon/${name.toLowerCase()}');
    Map jsonObj = jsonDecode(tmp.body);
    //this.types = jsonObj['types'];
    this.spriteURL = jsonObj['sprites']['front_default'];
    this.hp = jsonObj['stats'][0]['base_stat'];
    this.attack = jsonObj['stats'][1]['base_stat'];
    this.defense = jsonObj['stats'][2]['base_stat'];
    this.speed = jsonObj['stats'][5]['base_stat'];
    this.specie = jsonObj['species']['url'];
    this.weight = jsonObj['weight'];
    this.height = jsonObj['height'];
    //this.height = jsonObj['order'];

    jsonObj['types'].forEach((f){
      this.types.add(f['type']['name']);
    });
    print('Pokemon: $name, types: $types');
    this.loaded = true;
    return this;
  }

  /*Future<Response> _fetchData(String s) async {
    //print('https://pokeapi.co/api/v2/pokemon/$s');
    
  }*/
}