import 'package:flutter/material.dart';
import 'package:pokeapp/error.dart';
import 'package:pokeapp/myDrawer.dart';
import 'package:pokeapp/pokemon.dart';
import 'package:pokeapp/pokemonDao.dart';
import 'comparison.dart';
import 'database.dart';
import 'globals.dart' as globals;
import 'pokeViewPage.dart';
import 'list.dart';
//import 'database.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final database = await $FloorFlutterDatabase
      .databaseBuilder('flutter_database.db')
      .build();
  final dao = database.pokemonDao;
  await globals.getTypes();
  runApp(MyApp(dao));
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  const MyApp(this.dao);
  final PokemonDao dao;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      initialRoute: '/',
      routes:{
        '/' : (context) => MyHomePage(title:'PokeApp', dao:dao),
        '/pokemon' : (context) => PokeViewPage(title: '${globals.selectedPokemon}', dao:dao),
        '/list' : (context) => PokeList(title: 'Pokedex', dao: dao,),
        '/comparison' : (context) => PokeComparison(title: 'Compare', dao: dao,),
        '/error' : (context) => ErrorPage(title: 'ErrorPage', dao: dao,)
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.dao}) : super(key: key);

  final String title;
  final PokemonDao dao;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    globals.updateFavourites(widget.dao);
    super.initState();
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: myDrawer(context, this, widget.dao, _controller),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
                onPressed: (){
                  //globals.selectedPokemon = "Pidgey";
                  Navigator.of(context).pushNamed('/list');
                } ,
                child: Container(
                  width: 200,
                  height: 50,
                  /*decoration: BoxDecoration(
                      border: Border.all(color: Colors.black)),*/
                  child: Center(
                      child: Text('Pokedex',
                          style: TextStyle(fontSize: 20),
                          textAlign: TextAlign.center)),
                )),
                Container(height:15)
                ,
                RaisedButton(
                onPressed: (){
                  Navigator.of(context).pushNamed('/comparison');
                } ,
                child: Container(
                  width: 200,
                  height: 50,
                  /*decoration: BoxDecoration(
                      border: Border.all(color: Colors.black)),*/
                  child: Center(
                      child: Text('Comparison',
                          style: TextStyle(fontSize: 20),
                          textAlign: TextAlign.center)),
                ))
          ],
        ),
      ),
    );
  }
}
