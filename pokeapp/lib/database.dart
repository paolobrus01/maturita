import 'dart:async';

import 'pokemon.dart';
import 'pokemonDao.dart';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'database.g.dart';

@Database(version: 1, entities: [Pokemon])
abstract class FlutterDatabase extends FloorDatabase {
  PokemonDao get pokemonDao;
}