import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pokeapp/myDrawer.dart';
import 'package:pokeapp/pokemon.dart';
import 'globals.dart' as globals;
import 'pokeViewPage.dart';
import 'pokemonDao.dart';

class PokeComparison extends StatefulWidget {
  PokeComparison({Key key, this.title, this.dao}) : super(key: key);

  final String title;
  final PokemonDao dao;

  @override
  _PokeComparisonState createState() => _PokeComparisonState();
}

class _PokeComparisonState extends State<PokeComparison> {
  TextEditingController _controllerA = TextEditingController();
  TextEditingController _controllerB = TextEditingController();
  String _filterA;
  String _filterB;

  Future<PokemonAdvanced> _pokemonA;
  Future<PokemonAdvanced> _pokemonB;

  TextEditingController drawerController = TextEditingController();

  StreamSubscription _streamSub;
  bool _loading = false;
  int _timer = 0;
  List<String> l = [], _filtredListA, _filtredListB;

  _getList() async {
    l = [];
    _filtredListA = [];
    _filtredListB = [];
    _streamSub = Stream<int>.periodic(Duration(milliseconds: 100), (x) => x)
        .listen((data) {
      if (!_loading)
      _streamSub.pause();
        setState(() {
          
        });
      _timer++;
      if (_timer >= 30) {
        //print('Error: Data cannot be get');
        Navigator.of(context).pushReplacementNamed('/error');
        _streamSub.pause();
      }
    });
    Response tmp = await get('https://pokeapi.co/api/v2/pokemon?limit=1000');
    Map<String, dynamic> jsonObj = jsonDecode(tmp.body);
    jsonObj['results'].forEach((pokemon) {
      l.add(pokemon['name'].toString());
    });
    _filtredListA = l;
    _filtredListB = l;
    _loading = false;
    setState(() {
      
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    _getList();
    _pokemonA = PokemonAdvanced.fromID('pikachu').load();
    _pokemonB = PokemonAdvanced.fromID('pikachu').load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        drawer: myDrawer(context, this, widget.dao, drawerController),
        appBar: AppBar(title: Text(widget.title)),
        body: _loading?
        Center(
          child: Container(
            width:50,
            height:50,
            child: CircularProgressIndicator(),
          ),
        ):
        Center(
            child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
                flex: 4,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FutureBuilder(
                      future: _pokemonA,
                      builder: (ctx, snapshot) {
                        //print(snapshot.data.types);
                        if (snapshot.hasData) {
                          return Column(children: [
                            Container(
                              width: 150,
                              height: 150,
                              child: Image.network(
                                snapshot.data.spriteURL,
                                fit: BoxFit.fill,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 0),
                              child: Text(
                                snapshot.data.name.toString().toUpperCase(),
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 16),
                              ),
                            ),
                          ]);
                        }
                        return Container(
                            width: 50,
                            height: 50,
                            child: CircularProgressIndicator());
                      },
                    ),
                    Column(children: [
                      Container(
                        width: 200,
                        height: 50,
                        padding: EdgeInsets.only(top: 10),
                        child: TextField(
                            controller: _controllerA,
                            textAlign: TextAlign.left,
                            decoration: InputDecoration(
                                hintText: 'Search',
                                prefixIcon: Icon(Icons.search),
                                alignLabelWithHint: true),
                            onChanged: (data) {
                              setState(() {
                                _filterA = data.toUpperCase();
                                _filtredListA = [];
                                _filtredListA = l.where((test) {
                                  return (test ?? '')
                                      .toUpperCase()
                                      .startsWith(_filterA);
                                }).toList();
                              });
                            }),
                      ),
                      Container(
                        height: 150,
                        width: 200,
                        child: ListView.builder(
                            itemCount: _filtredListA.length,
                            itemBuilder: (ctx, index) {
                              return ListTile(
                                  title: Text(
                                    _filtredListA[index],
                                    textAlign: TextAlign.center,
                                  ),
                                  onTap: () {
                                    setState(() {
                                      _pokemonA = PokemonAdvanced.fromID(
                                              _filtredListA[index])
                                          .load();
                                    });
                                  });
                            }),
                      )
                    ])
                  ],
                )),
            Flexible(
                flex: 3,
                child: Row(
                  
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    //STATS POKEMON A
                    children: [
                      Row(children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Card(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(MdiIcons.hospital),
                                    Container(
                                      width: 5,
                                    ),
                                    FutureBuilder(
                                        future: _pokemonA,
                                        builder: (ctx, snapshot) {
                                          if (snapshot.hasData)
                                            return Text(
                                                snapshot.data.hp.toString());
                                          return Text('???');
                                        })
                                  ],
                                ),
                              ),
                            ),
                            Card(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(MdiIcons.sword),
                                    Container(
                                      width: 5,
                                    ),
                                    FutureBuilder(
                                        future: _pokemonA,
                                        builder: (ctx, snapshot) {
                                          if (snapshot.hasData)
                                            return Text(snapshot.data.attack
                                                .toString());
                                          return Text('???');
                                        })
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Card(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(MdiIcons.shield),
                                    Container(
                                      width: 5,
                                    ),
                                    FutureBuilder(
                                        future: _pokemonA,
                                        builder: (ctx, snapshot) {
                                          if (snapshot.hasData)
                                            return Text(snapshot.data.defense
                                                .toString());
                                          return Text('???');
                                        })
                                  ],
                                ),
                              ),
                            ),
                            Card(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(MdiIcons.runFast),
                                    Container(
                                      width: 5,
                                    ),
                                    FutureBuilder(
                                        future: _pokemonA,
                                        builder: (ctx, snapshot) {
                                          if (snapshot.hasData)
                                            return Text(
                                                snapshot.data.speed.toString());
                                          return Text('???');
                                        })
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ]),
                      Container(
                        child: FutureBuilder(
                          future: _pokemonA,
                          builder: (ctx, snapshot){
                            if(snapshot.hasData){
                              return FutureBuilder(
                                future: _pokemonB,
                                builder: (ctx2, snapshot2){
                                  if(snapshot2.hasData)
                                  return Text(
                                    globals.compareTypes(
                                    snapshot.data, snapshot2.data
                                  ));

                                  
                                  return Text('Waiting');
                                  
                                },
                                
                                
                                );
                            }
                              return Container();
                            }
                          
                          ),
                      ),
                      Row(
                        children:[
                          Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Card(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(MdiIcons.hospital),
                                  Container(
                                    width: 5,
                                  ),
                                  FutureBuilder(
                                      future: _pokemonB,
                                      builder: (ctx, snapshot) {
                                        if (snapshot.hasData)
                                          return Text(
                                              snapshot.data.hp.toString());
                                        return Text('???');
                                      })
                                ],
                              ),
                            ),
                          ),
                          Card(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(MdiIcons.sword),
                                  Container(
                                    width: 5,
                                  ),
                                  FutureBuilder(
                                      future: _pokemonB,
                                      builder: (ctx, snapshot) {
                                        if (snapshot.hasData)
                                          return Text(
                                              snapshot.data.attack.toString());
                                        return Text('???');
                                      })
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Card(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(MdiIcons.shield),
                                  Container(
                                    width: 5,
                                  ),
                                  FutureBuilder(
                                      future: _pokemonB,
                                      builder: (ctx, snapshot) {
                                        if (snapshot.hasData)
                                          return Text(
                                              snapshot.data.defense.toString());
                                        return Text('???');
                                      })
                                ],
                              ),
                            ),
                          ),
                          Card(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(MdiIcons.runFast),
                                  Container(
                                    width: 5,
                                  ),
                                  FutureBuilder(
                                      future: _pokemonB,
                                      builder: (ctx, snapshot) {
                                        if (snapshot.hasData)
                                          return Text(
                                              snapshot.data.speed.toString());
                                        return Text('???');
                                      })
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                        ]
                      )
                    ])),
            Flexible(
                flex: 5,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    
                    Column(children: [
                      Container(
                        width: 200,
                        height: 50,
                        padding: EdgeInsets.only(top: 10),
                        child: TextField(
                            controller: _controllerB,
                            textAlign: TextAlign.left,
                            decoration: InputDecoration(
                                hintText: 'Search',
                                prefixIcon: Icon(Icons.search),
                                alignLabelWithHint: true),
                            onChanged: (data) {
                              setState(() {
                                _filterB = data.toUpperCase();
                                _filtredListB = [];
                                _filtredListB = l.where((test) {
                                  return (test ?? '')
                                      .toUpperCase()
                                      .startsWith(_filterB);
                                }).toList();
                              });
                            }),
                      ),
                      Container(
                        height: 150,
                        width: 200,
                        child: ListView.builder(
                            itemCount: _filtredListB.length,
                            itemBuilder: (ctx, index) {
                              return ListTile(
                                  title: Text(
                                    _filtredListB[index],
                                    textAlign: TextAlign.center,
                                  ),
                                  onTap: () {
                                    setState(() {
                                      _pokemonB = PokemonAdvanced.fromID(
                                              _filtredListB[index])
                                          .load();
                                    });
                                  });
                            }),
                      )
                    ]),
                    FutureBuilder(
                      future: _pokemonB,
                      builder: (ctx, snapshot) {
                        //print(snapshot.data.types);
                        if (snapshot.hasData) {
                          return Column(children: [
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              child: Text(
                                snapshot.data.name.toString().toUpperCase(),
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 16),
                              ),
                            ),
                            Container(
                              width: 150,
                              height: 150,
                              child: Image.network(
                                snapshot.data.spriteURL,
                                fit: BoxFit.fill,
                              ),
                            )
                          ]);
                        }
                        return Container(
                            width: 50,
                            height: 50,
                            child: CircularProgressIndicator());
                      },
                    ),
                  ],
                ))
          ],
        )),
        resizeToAvoidBottomInset: false);
  }
}
