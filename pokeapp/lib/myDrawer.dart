import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:pokeapp/pokemon.dart';
import 'package:pokeapp/pokemonDao.dart';
import 'globals.dart' as globals;
import 'pokeViewPage.dart';

Drawer myDrawer(BuildContext context, dynamic page, PokemonDao dao, TextEditingController controller){

  Future<List<Pokemon>> _list = dao.findAllPokemons();
  String filter = controller.text.toUpperCase();
  

  return Drawer(
  // Add a ListView to the drawer. This ensures the user can scroll
  // through the options in the drawer if there isn't enough vertical
  // space to fit everything.
  child: ListView(
    // Important: Remove any padding from the ListView.
    padding: EdgeInsets.zero,
    children: <Widget>[
      DrawerHeader(
        child: Text('Pokèapp', style: TextStyle(color: Colors.white, fontSize: 20),),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
        
        ),

      ),
      ListTile(
        title: Text('Home'),
        trailing: Icon(Icons.home),
        onTap: () {
          Navigator.of(context).pushReplacementNamed('/');
        },
      ),
      ListTile(
        title: Text('Options'),
        trailing: Icon(Icons.settings),
        onTap: () {
          // Update the state of the app.
          // ...
        },
      ),
      
      ListTile(
        title: TextField(
          controller: controller,
          onChanged: (x){
            page.setState((){

            });
          },
          decoration: InputDecoration(hintText: 'Search favourites')
        ),
      ),
      FutureBuilder(
        future: _list,
        builder: (ctx, snap){
          if(snap.hasData){
            List filtredList = snap.data.where((Pokemon test) {
              print('${test.name} starts with ${controller.text.toUpperCase()}');
              return (test.name).toUpperCase().startsWith(controller.text.toUpperCase());
            }).toList();
            //print(filtredList);
            return Container(
              height: 400,
              child:ListView.builder(
              itemCount:filtredList.length,
              itemBuilder: (ctx, index){
                return ListTile(
                  title: Text(filtredList[index].name.toString()), 
                  onTap: (){
                    globals.selectedPokemon = filtredList[index].name.toString();
                    Navigator.of(context).pushReplacementNamed('/');
                    Navigator.of(context).pushNamed('/list');
                    Navigator.of(context).pushNamed('/pokemon');
                  },
                  );
              }));
          }
          return ListTile(title: Text("Loading..."),);
        })
    ],
  ),
);
}