import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:pokeapp/myDrawer.dart';
import 'package:pokeapp/pokemon.dart';
import 'globals.dart' as globals;
import 'pokeViewPage.dart';
import 'pokemonDao.dart';


class ErrorPage extends StatefulWidget {
  ErrorPage({Key key, this.title, this.dao}) : super(key: key);

  final String title;
  final PokemonDao dao;

  @override
  _ErrorPageState createState() => _ErrorPageState();
}

class _ErrorPageState extends State<ErrorPage> {

  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      drawer: myDrawer(context, this, widget.dao, controller),
      appBar: AppBar(
        title:Text(widget.title)
        ),
      body: Center(
        child:Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Check the connection\nand try again', style: TextStyle(fontSize: 16),)
          ],
        )
      ),
    );
  }

}